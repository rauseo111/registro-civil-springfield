import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(AllExceptionFilter.name);

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const meformat =
      exception instanceof HttpException ? exception.getResponse() : exception;
    let { message, name } = meformat;
    this.logger.error(`Status ${status} Error ${message} `);
    response
      .status(status)
      .json({
        time: new Date().toISOString(),
        path: request.url,
        error: { code: status, message, name },
      });
  }
}
