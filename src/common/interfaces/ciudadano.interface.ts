export interface Ciudadano extends Document {
  nombre: string;
  apellido: string;
  identificacion: string;
  genero: string;
  fechaNacimiento: Date;
  direccion: string;
  empleo: string;
  statusVida: string;
}
