import { Test, TestingModule } from '@nestjs/testing';
import { CIUDADANO } from 'src/common/models/model';
import { CiudadanoController } from './ciudadano.controller';
import { CiudadanoService } from './ciudadano.service';
import { getModelToken } from '@nestjs/mongoose';
const mappingModel = {
  create: jest.fn(),
  findAll: jest.fn(),
  updateID: jest.fn(),
  };
describe('CiudadanoService', () => {
  let service: CiudadanoService;
  let model: typeof mappingModel;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CiudadanoService, 
        {
          provide: getModelToken('ciudadano'), 
          useValue: mappingModel
    }],
    }).compile();

    service = module.get<CiudadanoService>(CiudadanoService);
    model = module.get(getModelToken('ciudadano'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(model).toBeDefined();
  });
});
