import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CIUDADANO } from 'src/common/models/model';
import { Ciudadano } from 'src/common/interfaces/ciudadano.interface';
import { CiudadanoDTO } from './dto/ciudadano.dto';

@Injectable()
export class CiudadanoService {
  constructor(
    @InjectModel(CIUDADANO.name) private readonly model: Model<Ciudadano>,
  ) {}

  async create(ciudadanoDTO: CiudadanoDTO): Promise<Ciudadano> {
    try {
      const { vigencia, edad } = this.esperanzaVida(
        ciudadanoDTO.fechaNacimiento,
      );
      if (vigencia) {
        const newCiudadano = new this.model({
          ...ciudadanoDTO,
          statusVida: 'F',
          edad,
        });
        return await newCiudadano.save();
      }
      const newCiudadano = new this.model({ ...ciudadanoDTO, edad });
      return await newCiudadano.save();
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: error,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async findAll(data: any): Promise<any> {
    try {
      const formaQuery = this.formatQuery(data);
      if (formaQuery.length > 0) {
      const response = await this.model.find({ $and: formaQuery });
        if (response.length > 0) {
            return response;
        } else {
          return {...response, message: 'no se encontraron registros asociados a la busquedad'};
        }
      } 
        return await this.model.find();
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: error,
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async updateID(id: string, ciudadanoDTO: CiudadanoDTO): Promise<Ciudadano> {
    try {
      let ciudadano = {};
      const { vigencia, edad } = this.esperanzaVida(
        ciudadanoDTO.fechaNacimiento,
      );
      if (vigencia) {
        ciudadano = { ...ciudadanoDTO, statusVida: 'F', edad };
        return await this.model.findByIdAndUpdate(id, ciudadano, { new: true });
      }
      ciudadano = { ...ciudadanoDTO, statusVida: 'V', edad };
      return await this.model.findByIdAndUpdate(id, ciudadano, { new: true });
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: error,
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  esperanzaVida(fecha: any): any {
    let validaVigencia: boolean = false;
    let hoy: Date = new Date();
    let cumpleanos = new Date(fecha);
    let edad: number = hoy.getFullYear() - cumpleanos.getFullYear();
    let m: number = hoy.getMonth() - cumpleanos.getMonth();
    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      edad--;
    }
    if (edad > 105) {
      validaVigencia = true;
    }
    return { vigencia: validaVigencia, edad };
  }

  formatQuery(data): Array<any> {
    let $regex = {};
    let fechaNacimiento = {};
    const queryCustoms: Array<any> = [];
    if (data.hasOwnProperty('fechaInicio') && data.hasOwnProperty('fechaFin')) {
      fechaNacimiento = { $gte: data['fechaInicio'], $lte: data['fechaFin'] };
      queryCustoms.push({ fechaNacimiento });
    }
    let { fechaInicio, fechaFin, ...newData } = data;
    for (let key in newData) {
      $regex = data[key];
      queryCustoms.push({ [key]: { $regex, $options: 'i' } });
    }
    return queryCustoms;
  }
}
