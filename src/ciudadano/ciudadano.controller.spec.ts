import { Test, TestingModule } from '@nestjs/testing';
import { CiudadanoController } from './ciudadano.controller';
import { CiudadanoService } from './ciudadano.service';
import { getModelToken } from '@nestjs/mongoose';

describe('CiudadanoController', () => {
  let controller: CiudadanoController;
  let service: CiudadanoService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CiudadanoController],
      providers: [
        CiudadanoService,
        {
          provide: getModelToken('ciudadano'),
          useValue: {Symbol: jest.fn()}
        }
      ]
    }).compile();

    controller = module.get<CiudadanoController>(CiudadanoController);
    service = module.get<CiudadanoService>(CiudadanoService);
  });

  it('debe ser definido', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });
});
