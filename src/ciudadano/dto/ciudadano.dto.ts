import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
export class CiudadanoDTO {
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  public readonly nombre: string;
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  readonly apellido: string;
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  readonly identificacion: string;
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  readonly genero: string;
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsDateString()
  @IsOptional()
  readonly fechaNacimiento: Date;
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  readonly direccion: string;
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  readonly empleo: string;
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly statusVida: string;
  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  readonly edad: number;
  @ApiPropertyOptional()
  @IsDateString()
  @IsOptional()
  readonly fechaInicio: Date;
  @ApiPropertyOptional()
  @IsDateString()
  @IsOptional()
  readonly fechaFin: Date;
}
