import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { CiudadanoService } from './ciudadano.service';
import { CiudadanoDTO } from './dto/ciudadano.dto';

@Controller('springfield/citizens')
export class CiudadanoController {
  private readonly logger = new Logger();
  constructor(private readonly ciudadanoService: CiudadanoService) {}
  @Post('create')
  @ApiOperation({ summary: 'Creacion ciudadano' })
  async create(@Res() response, @Body() ciudadanoDTO: CiudadanoDTO) {
    try {
      const respCreate = await this.ciudadanoService.create(ciudadanoDTO);
      return response
        .status(HttpStatus.OK)
        .json({
          code: HttpStatus.OK,
          time: new Date().toISOString(),
          data: respCreate,
        });
    } catch (error) {
      this.logger.error(`${error.response.error}`);
      return response
        .status(error.response.status)
        .json({
          time: new Date().toISOString(),
          error: {
            code: error.response.status,
            message: 'Error al crear registro ciudadano',
          },
        });
    }
  }

  @Get('all')
  @ApiOperation({ summary: 'Listado Ciudadanos' })
  async findAll(@Res() response, @Query() query: CiudadanoDTO) {
    try {
      const respAll = await this.ciudadanoService.findAll(query);
      return response
        .status(HttpStatus.OK)
        .json({
          code: HttpStatus.OK,
          time: new Date().toISOString(),
          data: respAll,
        });
    } catch (error) {
      this.logger.error(`${error.response.error}`);
      return response
        .status(error.response.status)
        .json({
          time: new Date().toISOString(),
          error: {
            code: error.response.status,
            message: 'Error al listar Ciudadanos',
          },
        });
    }
  }

  @Put('update/:id')
  @ApiOperation({ summary: 'Actualiza registro ciudadanno' })
  async updateID(
    @Res() response,
    @Param('id') id: string,
    @Body() ciudadanoDTO: CiudadanoDTO,
  ) {
    try {
      const respUpdate = await this.ciudadanoService.updateID(id, ciudadanoDTO);
      return response
        .status(HttpStatus.OK)
        .json({
          code: HttpStatus.OK,
          time: new Date().toISOString(),
          data: respUpdate,
        });
    } catch (error) {
      this.logger.error(`${error.response.error}`);
      return response
        .status(error.response.status)
        .json({
          time: new Date().toISOString(),
          error: {
            code: error.response.status,
            message: 'Error al actualizar Ciudadano',
          },
        });
    }
  }
}
