import * as mongoose from 'mongoose';

export const CiudadanoSchema = new mongoose.Schema(
  {
    nombre: { type: String, required: true },
    apellido: { type: String, required: true },
    identificacion: { type: String, required: true },
    genero: { type: String, required: true },
    fechaNacimiento: { type: String, required: true },
    direccion: { type: String, required: true },
    empleo: { type: String, required: true },
    statusVida: { type: String, required: false, default: 'V' },
    edad: { type: Number, required: false },
  },
  {
    timestamps: true,
  },
);

CiudadanoSchema.index({ identificacion: 1 }, { unique: true });
