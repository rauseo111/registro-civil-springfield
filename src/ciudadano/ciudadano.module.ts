import { Module } from '@nestjs/common';
import { CiudadanoController } from './ciudadano.controller';
import { CiudadanoService } from './ciudadano.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CIUDADANO } from 'src/common/models/model';
import { CiudadanoSchema } from 'src/ciudadano/schema/ciudadano.schema';
@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: CIUDADANO.name,
        useFactory: () => {
          return CiudadanoSchema;
        },
      },
    ]),
  ],
  controllers: [CiudadanoController],
  providers: [CiudadanoService],
})
export class CiudadanoModule {}
